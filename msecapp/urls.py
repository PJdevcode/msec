from django.urls import path
from msecapp import views

urlpatterns = [
    path('', views.index),
    path('about',views.about),
    path('form',views.formdata),
    path('edit/<person_id>',views.edit),
    path('delete/<person_id>',views.delete)      
]